from django.db import models


class Note(models.Model):
    toNote = models.CharField(max_length=30, default="")

    fromNote = models.CharField(max_length=30, default="")
    title = models.TextField(default="")
    message = models.TextField(default="")
