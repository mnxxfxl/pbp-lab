import 'package:flutter/material.dart';
import 'package:lab_6/main.dart';
import 'package:lab_6/screens/bookmarks_screen.dart';

class MainDrawer extends StatelessWidget {
  const MainDrawer({Key? key}) : super(key: key);

  Widget buildListTile(String title, IconData icon, VoidCallback tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
      ),
      title: Text(
        title,
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            alignment: Alignment.center,
            color: Colors.pink,
            child: Text(
              'Modules',
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.home, () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => MyHomePage(),
              ),
            );
          }),
          buildListTile('Tasks', Icons.announcement, () {
            Navigator.pop(context);
          }),
          buildListTile('Recipes', Icons.shopping_cart, () {
            Navigator.pop(context);
          }),
          buildListTile('Bookmarks', Icons.bookmark, () {
            Navigator.push(
                context,
            MaterialPageRoute(
              builder: (_) => BookmarksScreen(),
            ));
          }),
          buildListTile('Motivation', Icons.lightbulb, () {
            Navigator.pop(context);
          }),
          buildListTile('Log In', Icons.login, () {
            Navigator.pop(context);
          }),
        ],
      ),
    );
  }
}
