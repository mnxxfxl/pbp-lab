import 'package:flutter/material.dart';
import 'package:lab_6/widgets/main_drawer.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Protes App',
      theme: ThemeData(
        brightness: Brightness.dark,
        fontFamily: "Rubik",
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: const Text("Protes Home"),
      ),
      drawer: const MainDrawer(),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RichText(
                textAlign: TextAlign.center,
                text: TextSpan(
                    text: "A place where you can keep up with your\n",
                    style: Theme.of(context).textTheme.headline3,
                    children: const <TextSpan>[
                      TextSpan(
                          text: "productivity",
                          style: TextStyle(fontStyle: FontStyle.italic)),
                    ])),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  padding: const EdgeInsets.all(15),
                    child: (OutlinedButton(
                  child: const Text("Log In"),
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all(Colors.white),
                    foregroundColor: MaterialStateProperty.all(Colors.black),
                    padding:
                        MaterialStateProperty.all(const EdgeInsets.symmetric(
                      vertical: 20,
                      horizontal: 40,
                    ))),
                  onPressed: () {},
                ))),
                Container(
                  padding: const EdgeInsets.all(15),
                    child: (OutlinedButton(
                  child: const Text("Sign Up"),
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.black),
                      foregroundColor: MaterialStateProperty.all(Colors.white),
                      padding:
                          MaterialStateProperty.all(const EdgeInsets.symmetric(
                        vertical: 20,
                        horizontal: 40,
                      ))),
                  onPressed: () {},
                )))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
