import "package:flutter/material.dart";
import 'package:lab_6/widgets/main_drawer.dart';

class BookmarksScreen extends StatefulWidget {
  const BookmarksScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BookmarksScreenState();
}

class _BookmarksScreenState extends State<BookmarksScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: const Text("Bookmarks"),
      ),
      drawer: const MainDrawer(),
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 20),
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                text: "Hello!\n",
                style: Theme.of(context).textTheme.headline4,
                children: const <TextSpan>[
                  TextSpan(
                    text: "Ready to store some more\n",
                  ),
                  TextSpan(
                    text: "knowledge?",
                    style: TextStyle(fontStyle: FontStyle.italic),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: ListTile(
              leading: const Icon(Icons.search),
              title: const TextField(
                decoration: InputDecoration(
                  hintText: "Search bookmarks",
                  border: InputBorder.none,
                ),
              ),
              trailing: IconButton(
                icon: const Icon(Icons.cancel),
                onPressed: () {
                },
              ),
            ),
          ),
          SizedBox(
            width: double.infinity,
            child: DataTable(columns: const <DataColumn>[
              DataColumn(
                  label: Text(
                "Name",
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
              DataColumn(
                  label: Text(
                "Description",
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
              DataColumn(
                  label: Text(
                "Date Added",
                style: TextStyle(fontWeight: FontWeight.bold),
              )),
            ], rows: const <DataRow>[
              DataRow(
                cells: <DataCell>[
                  DataCell(Text("Bookmark 1")),
                  DataCell(Text("Bookmark Lorem ipsum")),
                  DataCell(
                    Text("November 18, 2021"),
                  ),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text("Bookmark 2")),
                  DataCell(Text("Bookmark Lorem ipsum")),
                  DataCell(
                    Text("November 18, 2021"),
                  ),
                ],
              ),
              DataRow(
                cells: <DataCell>[
                  DataCell(Text("Bookmark 3")),
                  DataCell(Text("Bookmark Lorem ipsum")),
                  DataCell(
                    Text("November 18, 2021"),
                  ),
                ],
              ),
            ]),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: Colors.pink,
        child: const Icon(Icons.add),
      ),
    );
  }
}
