Apakah perbedaan antara JSON dan XML?
 - XML adalah _markup language_ yang menggunakan _tag_. JSON tidak menggunakan _tag_.
 - JSON mudah dibaca dengan mata dibandingkan dengan XML.
 - JSON tidak men-support _namespace_, beda dengan XML yang men-support.
 - JSON men-support _array_, beda dengan XML yang tidak men-support _array_.
 - JSON kurang _secure_ dari XML.
 - JSON tidak bisa mengandung komentar, beda dengan XML yang dapat mengandung komentar.
 - JSON hanya memakai _encoding UTF-8_, beda dengan XML yang dapat memakai berbagai macam _encoding_.


Apakah perbedaan antara HTML dan XML?
 - HTML didesain untuk menampilkan data, sedangkan XML didesain untuk mentransfer data.
 - HTML static, sedangkan XML dynamic.
 - Tag pada HTML telah didefinisi, sedangkan tag XML dibuat pengguna langsung.